using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Join_Pos : MonoBehaviour
{
    Vector3 PosLeftShoulder;
    Vector3 PosLeftWrist;
    Vector3 PosLeftElbow;

    Vector3 PosRightShoulder;
    Vector3 PosRightWrist;
    Vector3 PosRightElbow;

    Vector3 RightElbowWrist;
    Vector3 RightElbowShoulder;

    Vector3 LeftElbowWrist;
    Vector3 LeftElbowShoulder;

    public double LeftElbowJointAngle;
    public double RightElbowJointAngle;

    //double maxJointAngleRight = 0f;
    //double maxJointAngleLeft = 0f;

    // Start is called before the first frame update
    void Start()
    {
    gameObject.transform.localScale = new Vector3(
            -gameObject.transform.localScale.x,
            gameObject.transform.localScale.y,
            gameObject.transform.localScale.z);   
        
    }

    // Update is called once per frame
    void Update()
    {

        //Left Arm
        PosLeftWrist = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/" +
            "Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm/Character1_LeftHand").transform.position;

        PosLeftElbow = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm").transform.position;

        PosLeftShoulder = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm").transform.position;


        //Right Arm
        PosRightWrist = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/" +
            "Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm/Character1_RightHand").transform.position;

        PosRightElbow = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm").transform.position;

        PosRightShoulder = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm").transform.position;

        //Calculate Vectors
        RightElbowWrist = PosRightWrist - PosRightElbow;
        RightElbowShoulder = PosRightShoulder - PosRightElbow;

        LeftElbowWrist = PosLeftWrist - PosLeftElbow;
        LeftElbowShoulder = PosLeftShoulder - PosLeftElbow;

        //Calcualte angles
        LeftElbowJointAngle = 180 - Vector3.Angle(LeftElbowWrist, LeftElbowShoulder);
        RightElbowJointAngle = 180 - Vector3.Angle(RightElbowWrist, RightElbowShoulder);

        //Debug.Log("Right Joint Angle: " + RightElbowJointAngle);
        //Debug.Log("Left Joint Angle: " + LeftElbowJointAngle);

    //    if (maxJointAngleLeft<LeftElbowJointAngle)
    //    {
    //        maxJointAngleLeft = LeftElbowJointAngle;
    //    }

    //    if (maxJointAngleRight < RightElbowJointAngle)
    //    {
    //        maxJointAngleRight = RightElbowJointAngle;
    //    }
    //}

    //private void OnGUI()
    //{
    //    GUI.skin.box.fontSize = 22;
    //    GUI.Box(new Rect(400, 5, 300, 80), "Max Joint Angle Left: " + maxJointAngleLeft + "\nMax Joint Angle Right: " + maxJointAngleRight);



    }

}
