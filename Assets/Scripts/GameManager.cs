using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    public static float score = 0;
    public static int total_hits = 0;
    public static int total_miss = 0;
    public static int total_fail = 0;
    public static int balls = 0;
    public static int bombs = 0;
    private int failureScore = 10;
    private int Health = 10;
    public static float Gametime = 60f;

    public static double maxJointAngleRight;
    public static double maxJointAngleLeft;

    public static double minJointAngleRight = 180;
    public static double minJointAngleLeft = 180;

    double currentJointAngleLeft;
    double currentJointAngleRight;


    public void IncreaseScore()
    {
        score+=1f;
        total_hits++;      
    }

    public void IncreaseScoreY()
    {
        score += 0.5f;
        total_hits++;
    }

    public void IncreaseScoreR()
    {
        score += 0.1f;
        total_hits++;
    }

    public void ReduceScore()
    {
        score--;
        total_fail++;
    }

    public void Ball()
    {
        balls++;
    }

    public void Bomb()
    {
        bombs++;
    }

    public void FailureScore()

    {
       failureScore--;
        Health -= failureScore;
        if (failureScore == 0)
        {
           SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        GameObject theAvatar = GameObject.Find("Avatar(Clone)");
        Join_Pos jointPos = theAvatar.GetComponent<Join_Pos>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(score);
        //Debug.Log("Failure Score: " + failureScore);
        //Debug.Log("Score: "+ scoreText.text);
        GameObject theAvatar = GameObject.Find("Avatar(Clone)");
        Gametime -= Time.deltaTime;
        if (Gametime<=0)
        {
            total_miss = balls - total_hits;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        //Join_Pos jointPos = theAvatar.GetComponent<Join_Pos>();

        //currentJointAngleLeft = jointPos.LeftElbowJointAngle;
        //currentJointAngleRight = jointPos.RightElbowJointAngle;

        //if (maxJointAngleLeft < currentJointAngleLeft)
        //{
        //    maxJointAngleLeft = currentJointAngleLeft;
        //}

        //if ((currentJointAngleLeft > 0) && (minJointAngleLeft > currentJointAngleLeft))
        //{
        //    minJointAngleLeft = currentJointAngleLeft;
        //}


        //if ((currentJointAngleRight > 0) && (minJointAngleRight > currentJointAngleRight))
        //{
        //     minJointAngleRight = currentJointAngleRight; 
        //}

        //if (maxJointAngleRight < currentJointAngleRight)
        //{
        //    maxJointAngleRight = currentJointAngleRight;
        //}
       
    }

    private void OnGUI()
    {
        GUI.skin.box.fontSize = 25;
        GUI.color = Color.white;
        GUI.skin.box.alignment = TextAnchor.MiddleLeft;
        GUI.Box(new Rect(5, 5, 320, 80), "Score: " + score+ "\nTime Remaining: " + Math.Round(Gametime) +"s"); // + "\nRemaining lives: " + failureScore + "\nJoint Angle Left: " + Math.Round(currentJointAngleLeft,2) + "\nJoint Angle Right: " + Math.Round(currentJointAngleRight,2));
        //GUI.Box(new Rect(Screen.width - 375, 5, 370, 120), "Max Joint Angle Left: " + Math.Round(maxJointAngleLeft, 2) + "\nMax Joint Angle Right: " + Math.Round(maxJointAngleRight,2) 
        //    + "\nMin Joint Angle Left: " + Math.Round(minJointAngleLeft, 2) + "\nMin Joint Angle Right: " + Math.Round(minJointAngleRight, 2));
    }
}
