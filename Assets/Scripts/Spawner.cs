using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Vector3 Shoulder_Midpoint; //= new Vector3(0.07000031f,0.1190407f,-0.05289974f);
    public Vector3 ArmLength; //= new Vector3(1.5f,1.5f,1.5f);
    
    private Vector3 newSpawnerPosition;

    public Collider spawnArea;


    public GameObject[] fruitPrefabs;
    public GameObject spawn_;

    public float minSpawnDelay = 0.25f;
    public float maxSpawnDelay = 2.5f;

    //public float minAngle = -15f;
    //public float maxAngle = 15f;

    //public float minForce = 18f;
    //public float maxForce = 22f;

    //public float distance = 22.5f;

    private void Awake()
    {
        spawnArea = GetComponent<Collider>();
        spawn_ = GameObject.Find("Spawner");
        Shoulder_Midpoint = new Vector3(0.07000031f,0.1190407f,-0.05289974f);
        spawn_.transform.position = Shoulder_Midpoint;
        ArmLength = new Vector3(1f,1f,1f);
        spawn_.transform.localScale = ArmLength;
        Debug.LogWarning(Shoulder_Midpoint);
        Debug.LogWarning(ArmLength);
        Debug.LogWarning(spawnArea.bounds);
    }

    private void Start()
    {

    }

    private void Update()
    {
        Calc_Aux();
        Debug.LogWarning(Shoulder_Midpoint);
        //Debug.LogWarning(ArmLength);
        spawn_.transform.position = Shoulder_Midpoint;
    }

    private void OnEnable()
    {
        StartCoroutine(Spawn());
    }

    public void Calc_Aux()
    {
        Vector3 Left_Shoulder = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder").transform.position;
        Vector3 Right_Shoulder = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder").transform.position;
        //Vector3 Left_Arm = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm").transform.position;
        //Vector3 Right_Arm = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm").transform.position;
        //Vector3 Left_ForeArm = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm").transform.position;
        //Vector3 Right_ForeArm = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm").transform.position;
        //Vector3 Left_Hand = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm/Character1_LeftHand").transform.position;
        //Vector3 Right_Hand = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm/Character1_RightHand").transform.position;

        //float Arm = ((Distance(Left_Shoulder,Left_Arm)+Distance(Left_Arm,Left_ForeArm)+Distance(Left_ForeArm,Left_Hand))+(Distance(Right_Shoulder,Right_Arm)+Distance(Right_Arm,Right_ForeArm)+Distance(Right_ForeArm,Right_Hand)))/2;
        //ArmLength = new Vector3(Arm,Arm,Arm);
        Shoulder_Midpoint = (Left_Shoulder + Right_Shoulder) / 2;
    }

    public float Square(float value)
    {
        return value * value;
    }

    public float Distance(Vector3 from, Vector3 to)
    {
        return Mathf.Sqrt(
            Square(from.x - to.x) +
            Square(from.y - to.y) +
            Square(from.z - to.z)
        );
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public IEnumerator Spawn()
    {
        yield return new WaitForSeconds(3f);

        while (enabled)
        {
            int typ = Random.Range(0, 2);
            if(typ == 0)
                {
                GameObject prefab = fruitPrefabs[0];//Random.Range(0, fruitPrefabs.Length)

                Vector3 position = new Vector3();
                position.x = Random.Range(spawnArea.bounds.center.x - 0.65f, spawnArea.bounds.center.x + 0.65f);
                position.y = Random.Range(spawnArea.bounds.center.y - 0.45f, spawnArea.bounds.center.y + 0.65f);
                position.z = Random.Range(spawnArea.bounds.center.z - 0.65f, spawnArea.bounds.center.z);

                Quaternion rotation = Quaternion.Euler(0f, 0f, 0f);
                GameObject fruit = Instantiate(prefab, position, rotation);
            }
            if (typ == 1)
            {
                GameObject prefab = fruitPrefabs[1];//Random.Range(0, fruitPrefabs.Length)

                Vector3 position = new Vector3();
                position.x = Random.Range(spawnArea.bounds.center.x - 0.65f, spawnArea.bounds.center.x + 0.65f);
                position.y = Random.Range(spawnArea.bounds.center.y - 0.45f, spawnArea.bounds.center.y + 0.65f);
                position.z = Random.Range(spawnArea.bounds.center.z - 0.65f, spawnArea.bounds.center.z);

                Quaternion rotation = Quaternion.Euler(0f, 0f, 0f);
                GameObject bomb = Instantiate(prefab, position, rotation);
            }


            
            //Debug.LogWarning(fruit.transform.position);

            

            yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
        }
    }
   
}