using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCollision : MonoBehaviour
{
    public Material bombMaterial;

    public float LifeSpan;// = Random.Range(8f, 10f);
    public GameObject bomb;
    public float StartTime;

    private Collider bombCollider;
    //private Renderer surface;
    // Start is called before the first frame update

    void Awake()
    {
        LifeSpan = Random.Range(0.5f, 1f);
        //fruit = GameObject.Find("gameObject");
        FindObjectOfType<GameManager>().Bomb();
        Destroy(gameObject, LifeSpan);
    }

    void Start()
    {
        bombCollider = GetComponent<Collider>();
        //surface = GetComponent<MeshRenderer>();
        StartTime = Time.time;
        //fruitMaterial = GetComponent<Apple_Outside>();

        // Create a material with transparent diffuse shader
        bombMaterial = new Material(Shader.Find("Standard"));
        bombMaterial.color = Color.green;

        // assign the material to the renderer
        GetComponent<Renderer>().material = bombMaterial;
        gameObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - StartTime > 0.25f & bombMaterial.color == Color.green)
        {
            bombMaterial = new Material(Shader.Find("Standard"));
            bombMaterial.color = Color.cyan;
            GetComponent<Renderer>().material = bombMaterial;
            //gameObject.transform.localScale = new Vector3(0.20f,0.20f,0.20f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<GameManager>().ReduceScore();
            //fruitCollider.enabled = false;
            //surface.enabled=false;
            Destroy(gameObject);
        }
    }
}
