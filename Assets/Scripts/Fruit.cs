using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// mine
public class Fruit : MonoBehaviour
{

    public GameObject whole;
    private Rigidbody fruitRigidbody;
    private Collider fruitCollider;
    private Renderer surface;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            FindObjectOfType<GameManager>().IncreaseScore();
            fruitCollider.enabled = false;
            whole.SetActive(false);
            surface = GetComponent<MeshRenderer>();
            surface.enabled = false;
            Destroy(whole);

            //Destroy(whole);
        }
    }

    //void OnCollisionEnter(Collision hit)
    //{
    //    if (hit.gameObject.name == "DetectionPlate")
    //        FindObjectOfType<GameManager>().FailureScore();
    //}



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
